﻿/*
 * Name: Tristan Cassidy
 * Date: 4/29/2022
 * Purpose: To cause the enemies to chase the player if they look at them for too long.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    
    public Transform player;
    public int speed;

    public int enemyAnger;
    // Start is called before the first frame update
    void Start()
    {
        enemyAnger = 0;
    }

    // Update is called once per frame
    void Update()
    {

        if (enemyAnger > 300) //enemy anger in incremented in the player controller
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
        }
        
    }

}
