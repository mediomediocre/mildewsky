﻿/*
 * Name: Tristan Cassidy
 * Date: 4/29/2022
 * Purpose: To allow control of the player game object and implent the enemy functionality
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public CharacterController controller;
    public Transform groundCheck;
    public Transform playerLook;

    public float speed = 12f;
    public float gravity = -9.81f;
    public float groundDistance = 0.4f;
    public float jumpHeight = 3f;
    public LayerMask groundMask;
    public GameObject enemy;
    public GameObject tutorial;
    public GameObject winText;
    public GameObject TheySeeYou; //notification that the enemy is angry at the player

    private Vector3 velocity;
    private bool isGrounded;
    private EnemyController enemyCont;

    // Start is called before the first frame update
    void Start()
    {
        enemyCont = enemy.GetComponent<EnemyController>();
        GameManager.instance.DisableObject(winText);
        GameManager.instance.DisableObject(TheySeeYou);
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        Debug.DrawRay(playerLook.position, playerLook.TransformDirection(Vector3.forward) * 25, Color.red, 2f);
        RaycastHit hit;
        if (Physics.Raycast(playerLook.position, playerLook.TransformDirection(Vector3.forward), out hit, 25f))
        {
            if (hit.collider.CompareTag("Enemy")) //if the player looks at an enemy, they get mad, at 300 anger, they chase the player
            {
                ++enemyCont.enemyAnger;
                GameManager.instance.EnableObject(TheySeeYou); //UI element to tell player is looking at the enemy
            }
            else
            {
                if (enemyCont.enemyAnger > 0) //if player is looking away, anger will decrease
                {
                    --enemyCont.enemyAnger;
                    GameManager.instance.DisableObject(TheySeeYou);
                }
            }
        }

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }

        velocity.y += gravity * 2 * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        if (Input.anyKey) //disable the tutorial text if any input is made
        {
            GameManager.instance.DisableObject(tutorial);
        }
    }

    void OnTriggerEnter(Collider other) //win condition, when the player gets to the end this happens.
    {
        if (other.CompareTag("Win"))
        {
            GameManager.instance.EnableObject(winText);
        }
    }
}
