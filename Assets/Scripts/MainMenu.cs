﻿/*
 * Name: Tristan Cassidy
 * Date: 4/29/2022
 * Purpose: To add simple functionality to the main menu screen.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public string gameScene;
    public Button begin;

    // Start is called before the first frame update
    void Start()
    {
        begin.onClick.AddListener(StartGame);
    }

    

    public void StartGame()
    {
        SceneManager.LoadScene(gameScene);
    }
}
