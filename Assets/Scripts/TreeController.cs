﻿/*
 * Name: Tristan Cassidy
 * Date: 4/29/2022
 * Purpose: To add visual flair to the tree background elements if the player is close enough.
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeController : MonoBehaviour
{
    public enum TreeState { Idle, BeatIdle, Beat };
    public Transform playerTransform;
    public AudioSource heartBeat;
    public SpriteRenderer spriteRenderer;
    public Sprite idleSprite;
    public Sprite beatSprite;

    private int hasBeat;

    private TreeState currentState;
    // Start is called before the first frame update
    void Start()
    {
        currentState = TreeState.Idle;
        StartCoroutine(TreeIdle());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ChangeState(TreeState newState)
    {
        StopAllCoroutines();
        currentState = newState;

        switch (currentState)
        {
            case TreeState.Idle:
                Debug.Log("Tree is idle.");
                StartCoroutine(TreeIdle());
                break;
            case TreeState.Beat:
                Debug.Log("Tree is beating.");
                StartCoroutine(TreeBeat());
                break;
            case TreeState.BeatIdle:
                Debug.Log("Tree is beating (waiting to beat)");
                StartCoroutine(TreeBeatIdle());
                break;
        }
    }

    IEnumerator TreeIdle() //if the player isn't close, the tree will not beat
    {
        while (true)
        {
            float distanceFromPlayer = Vector3.Distance(transform.position, playerTransform.position);
            if (distanceFromPlayer < 30) //if the player is close enough, transition from idle to beat
            {
                hasBeat = 0;
                ChangeState(TreeState.Beat);
            } else
            {
                spriteRenderer.sprite = idleSprite;
            }
            yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator TreeBeat() //once the player gets close, start beating and alternate between sprites
    {
        while (true)
        {
            float distanceFromPlayer = Vector3.Distance(transform.position, playerTransform.position);
            spriteRenderer.sprite = beatSprite;
            if (hasBeat == 1) //if this has iterated once, transition to beat idle
            {
                hasBeat = 0;
                ChangeState(TreeState.BeatIdle);
            }
            if (distanceFromPlayer > 30) //if the player is far away, transition to idle
            {
                ChangeState(TreeState.Idle);
            }
            heartBeat.Play(); //play heart beat sound
            hasBeat = 1;
            yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator TreeBeatIdle() //alternate sprite given
    {
        while (true)
        {
            float distanceFromPlayer = Vector3.Distance(transform.position, playerTransform.position);
            spriteRenderer.sprite = idleSprite;
            if (hasBeat == 1) //if this has iterated once, transition to back to beat
            {
                hasBeat = 0;
                ChangeState(TreeState.Beat);
            }
            if (distanceFromPlayer > 30) //if the player is far away, transition to idle
            {
                hasBeat = 0;
                ChangeState(TreeState.Idle);
            }
            heartBeat.Play();
            hasBeat = 1;
            yield return new WaitForSeconds(1f);
        }
    }
}
