﻿/*
Name: Tristan Cassidy
Date: 4/29/2022
Purpose: To provide universal methods to the rest of the game objects.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    public void DisableObject(GameObject gameObject) //disable an object
    {
        gameObject.SetActive(false);
    }
    public void EnableObject(GameObject gameObject) //enable an object
    {
        gameObject.SetActive(true);
    }
}
