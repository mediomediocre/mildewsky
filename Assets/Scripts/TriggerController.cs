﻿/*
 * Name: Tristan Cassidy
 * Date: 4/29/2022
 * Purpose: To add functionality to the trigger game objects, which allow the enemy to appear throughout the game in specific areas once the player has passed a certain point.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerController : MonoBehaviour
{
    public GameObject trigger; //specifies which trigger this is.
    public Transform enemy;
    public Transform enemyLocation; //an empty gameobject
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            enemy.position = enemyLocation.position; //teleport enemy to empty
            trigger.SetActive(false); //trigger disappears
        }
    }
}
