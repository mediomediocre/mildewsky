﻿/*
 * Name: Tristan Cassidy
 * Date: 4/29/2022
 * Purpose: To add a lose condition if the player is to fall out of bounds or touch an enemy.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyCollision : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SceneManager.LoadScene(1);
        }
    }
}
